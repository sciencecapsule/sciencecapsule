from pathlib import Path
import shutil
import tempfile

import pytest

from actions import *


@pytest.fixture
def make_actions():
    class MyActions(ActionHandler):
        create_file = CreateFile(path='foo.txt')
        move_file = MoveFile(src_path=create_file.path, dst_path='bar.txt')

    def _make_actions():
        return MyActions()

    return _make_actions


@pytest.fixture
def actions_to_run():

    class MyOtherActions(ActionHandler):
        base_dir = Path(tempfile.mkdtemp())
        create_file = CreateFile(path=base_dir / 'foo.txt')
        write_to_file = WriteTextToFile(path=create_file.path, n_lines=10_000)
        move_file = MoveFile(src_path=create_file.path, dst_path=base_dir / 'bar.txt')
        copy_file = CopyFile(src_path=move_file.dst_path, dst_path=base_dir / 'bar-copy.txt')
        delete_file = DeleteFile(path=move_file.dst_path)
        add_content_to_copy = WriteTextToFile(wait_before=2, path=copy_file.dst_path, n_lines=1, mode='ta')

    obj = MyOtherActions()
    yield obj
    print(f'MyOtherActions.base_dir={MyOtherActions.base_dir}')
    # shutil.rmtree(base_dir)


def test_actions_are_run(actions_to_run):
    # actions_to_run = make_actions()
    actions_to_run.run()

    expected_content_after_running = [actions_to_run.add_content_to_copy.path]
    content = list(actions_to_run.base_dir.rglob('*'))
    assert content == expected_content_after_running
