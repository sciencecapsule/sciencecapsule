import dataclasses as dc
import datetime
import json
import functools
import os
from pathlib import Path
import time
import shutil


dataclass_immutable = functools.partial(dc.dataclass, frozen=True)


@dataclass_immutable
class _BaseAction:
    wait_before: float = None
    name: str = None


@dataclass_immutable
class PathOperation:
    path: Path = None


@dataclass_immutable
class PathToPathOperation:
    src_path: Path = None
    dst_path: Path = None


@dataclass_immutable
class CreateFile(PathOperation, _BaseAction):
    exist_ok: bool = False

    def __call__(self):
        self.path.touch(exist_ok=self.exist_ok)


@dataclass_immutable
class WriteRandomBytesToFile(PathOperation):
    size: int = None

    def get_content(self):
        return os.urandom(self.size)

    def __call__(self):
        content = self.get_content()
        self.path.write_bytes(content)


@dataclass_immutable
class WriteTextToFile(PathOperation, _BaseAction):
    n_lines: int = None
    mode: str = 'tw'

    def get_line(self, *args, **kwargs):
        data = {'timestamp': datetime.datetime.now().isoformat()}
        return json.dumps(data)

    def get_content(self):
        lines = [self.get_line().strip() + '\n' for _ in range(self.n_lines)]
        return str.join('', lines)

    def __call__(self):
        content = self.get_content()
        with self.path.open(self.mode) as f:
            f.write(content)
            f.flush()


@dataclass_immutable
class ReadFromFile(PathOperation, _BaseAction):

    mode: str = 'tr'

    def __call__(self):
        with self.path.open(self.mode) as f:
            data = f.read()


@dataclass_immutable
class DeleteFile(PathOperation, _BaseAction):

    def __call__(self):
        self.path.unlink()


@dataclass_immutable
class CopyFile(PathToPathOperation, _BaseAction):

    def __call__(self):
        shutil.copy2(self.src_path, self.dst_path)


@dataclass_immutable
class MoveFile(PathToPathOperation, _BaseAction):

    def __call__(self):
        self.src_path.replace(self.dst_path)


class ActionHandler:
    def __init_subclass__(cls):
        cls._register_actions()

    _action_map = {}

    @classmethod
    def _register_actions(cls):

        action_map = {}
        hierarchy_base_first = reversed(cls.__mro__)
        for c in hierarchy_base_first:
            print(c)
            for name, obj in c.__dict__.items():
                if isinstance(obj, _BaseAction):
                    print(f'found Action object {obj}')
                    if obj.name is None:
                        with_name = dc.replace(obj, name=name)
                        setattr(cls, name, with_name)
                    else:
                        with_name = obj
                    action_map[name] = with_name
                    # replace the original object from the class definition
                    # so that it matches the new one after assigning the name

        cls._action_map = action_map

    def __iter__(self):
        return iter(type(self)._action_map.values())

    def run(self):
        for action in self:
            if action.wait_before:
                time.sleep(action.wait_before)
            print(f'Running action {action}')
            action()

