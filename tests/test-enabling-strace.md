# Test Enabling Strace

This document is a checklist for testing the functionality used to enable strace for capturing process events on supported platforms.
It can be used as a reference to implement automatic tests, or for performing the tests manually.

### Test #1

- Setup of `strace` event source should be skipped if the Science Capsule container is started without the `sys_ptrace` capability

#### Procedure

- Run Science Capsule container without the `--cap-add SYS_PTRACE` flag

#### Expected

- Message displays that strace is not enabled because the container was started without the `sys_ptrace` capability
- `strace` is not added to `sources` in `events.yml` config file

### Test #2

- Setup of `strace` event source should be skipped if container is started with `sys_ptrace` capability, but the ptrace scope does not have the correct value

#### Procedure

- Set the `kernel.yama.ptrace_scope` to a value different than 0 following the instructions specific for each platform
- Run Science Capsule container with the `--cap-add SYS_PTRACE` flag

#### Expected

- Message displays that the container was created with the `sys_ptrace` capability, but the value of `kernel.yama.ptrace_scope` differs from required value
- Setup of `strace` event source is skipped

### Test #3

- If the container is started with the `sys_ptrace` capability and `kernel.yama.ptrace_scope` is set to the required value:
  - `strace` event source is configured correctly
  - Process events are captured correctly

#### Procedure

- Ensure that `kernel.yama.ptrace_scope` is set to `0`
- Run Science Capsule container with the `--cap-add SYS_PTRACE` flag

#### Expected

- Message displays:
  - The container was created with the `sys_ptrace` capability and `kernel.yama.ptrace_scope` has the required value
  - The PID of the current process will be added to the list of monitored PIDs
  - Log of the individual steps of `strace` setup
- `events.yml` config file is configured correctly
  - `monitored_pids` is a list containing a single value matching the value displayed during container startup
  - `sources` contains exactly one `strace` entry
- Event capture works as expected
  - `sc services start all` does not show errors
  - `$SC_CONFIG_DIR/services/logs/capture.log` does not show errors
  - After running a test process, e.g. `ls`:
    - Log of `capture` service shows that the process events (typically 2 for `start` and `exit`) were captured
    - `sc inspect events.process` displays the process events

### Test #4

- If `strace` is added to the event sources manually when the requirements are not fulfilled, event capture should fail gracefully, notify the user, and continue with the remaining sources after removing `strace`

#### Procedure

- Ensure that `kernel.yama.ptrace_scope` is set to a value other than 0
- Run Science Capsule container with the `--cap-add SYS_PTRACE` flag
- Inside the container, setup `strace` by editing the `events.yml` file:
  - `yq write --inplace "$SC_CONFIG_DIR/events.yml 'monitored_pids[+]' $$`
  - `yq write --inplace "$SC_CONFIG_DIR/events.yml 'source[+]' strace`
- Run `sc service restart capture`
- Generate events to test remaining sources, e.g. `echo asdf > foo.txt`

#### Expected

- No errors displayed during `sc service restart capture`
- The `capture` service logs contains:
  - Message notifying that issues were encountered when running strace monitor
  - Error message from `strace` monitor
  - Message notifying that the monitor was removed from the list of running monitors
  - Message notifying that the adaptor was removed from list of loaded adaptors
  - Confirmation that test events from the other sources were captured
  - No reference to `strace` event source after the messages above

### Test #5

- The steps to enable `strace` event capture for Docker on macOS should work as described in the docs

#### Procedure

- Display and note the current value of `kernel.yama.ptrace_scope` using a temporary container

```sh
docker run --rm --env SC_NO_AUTOSTART=1 sciencecapsule/base sysctl kernel.yama.ptrace_scope
```

- Set the `kernel.yama.ptrace_scope` to a different value (1 if 0, 0 if 1) using a temporary privileged container

```sh
docker run --rm --env SC_NO_AUTOSTART=1 --privileged --user root sciencecapsule/base sysctl -w kernel.yama.ptrace_scope=1
```

- Display the current value of `kernel.yama.ptrace_scope` as described above

#### Expected

- The value of `kernel.yama.ptrace_scope` has changed
