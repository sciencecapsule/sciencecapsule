# Contributing to Science Capsule

Thanks for your interest in Science Capsule. Our goal is to bring efficient, open-source software for enabling reproducible science to any and all researchers.


## Software Support

The easiest way to get started helping the project is to *file an issue*. If you encounter any bug or need any help setting up Science Capsule, please use the project's issue tracker, and create an issue if needed. You can do that on the issues page by clicking on the `Create issue` button at the top right corner. Issues can include bugs to fix, features to add, or documentation that looks outdated. It is recommended that you check if the issue has already been reported. Science Capsule's [issues are here](https://bitbucket.org/sciencecapsule/sciencecapsule/issues).


## Contributions

Contributions to Science Capsule code should be made in the form of pull requests. We are creating a pull request checklist and will update this section soon. Contributors should be able to follow the checklist and make appropriate pull requests once everything is in place.

### Pull Request Checklist

*Coming soon*