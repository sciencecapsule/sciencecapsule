pipeline { 
    agent any  

    options { disableConcurrentBuilds() }

    stages { 
       stage('Prepare Environment') {
            steps {
                script {
                    env.COMPONENT_NAME = "sciencecapsule"
                    env.BRANCH_NAME_NORM = env.BRANCH_NAME.replaceAll('/','_')
                    env.TAG_DEFAULT = "${env.BRANCH_NAME_NORM}_${env.BUILD_ID}"
                    env.PYTEST_ARGS = "--cov sc --cov-report xml:/artifacts/coverage.xml --junit-xml=/artifacts/test-report.xml"
                }
            }
        }

        stage('Build') { 
            steps { 
               sh("mkdir -p artifacts")
               sh("docker build -f jenkins/Dockerfile -t  ${env.TAG_DEFAULT} .")
               sh("docker run --rm --entrypoint pytest -v \$(pwd)/artifacts:/artifacts -t ${env.TAG_DEFAULT} " + env.PYTEST_ARGS)
            }
        }

        stage('Publish test result') {
            steps { 
               junit '**/artifacts/test-report.xml'
            }
        }

        stage('Publish coverage report') {
            steps { 
               step([$class: 'CoberturaPublisher', 
                     autoUpdateHealth: false, 
                     autoUpdateStability: false, 
                     coberturaReportFile: '**/artifacts/coverage.xml', 
                     failUnhealthy: false, 
                     failUnstable: false, 
                     maxNumberOfBuilds: 0, onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: true])
            }
        }

        stage('SonarQube analysis') {
            steps {
               sh("sed 's/<source>\\(\\(\\/[A-Za-z]*\\)\\)*<\\/source>/<source>sc<\\/source>/' < artifacts/coverage.xml  > artifacts/sonar_cov.xml")

               sh("sonar-scanner -Dsonar.language=py" +
                  " -Dsonar.python.coverage.reportPaths=artifacts/sonar_cov.xml" +
                  " -Dsonar.sources=sc" +
                  " -Dsonar.projectKey=${env.COMPONENT_NAME}_${env.TAG_DEFAULT}")
           }
       }
    }

    post { 
       always {
           script { 
              sh("docker image rm -f ${env.TAG_DEFAULT}")
      	   }
       } 
    }
}
