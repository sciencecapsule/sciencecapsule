import logging
import typing as t

from sc.db.models import connect as connect_db
from sc.capture.core import (
    Settings,
    CaptureManager
)
from sc.capture.util import register_stop_signals


LOG = logging.getLogger(__name__)


def run_capture(
        sources: t.Optional[t.List[str]] = None,
        settings: t.Optional[Settings] = None,
        monitor: t.Optional[bool] = None,
        ingest: bool = True
    ):
    settings = settings or Settings.from_config()
    capture_manager = CaptureManager()

    sources = sources or []
    capture_manager.init_sources(sources, settings)

    LOG.info('Connecting to DB')
    connect_db()

    if monitor:
        LOG.info('Running capture with monitoring')
        with capture_manager.setup_monitoring():
            capture_manager.run_forever(ingest=ingest)
    else:
        LOG.info('Running capture without monitoring')
        capture_manager.capture(ingest=ingest)
    LOG.info('Capture completed')
