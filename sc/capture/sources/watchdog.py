import os
import time

import pandas as pd
from watchdog import (
    events as _watchdog_events,
    observers as _watchdog_observers,
)

from sc.db import models
from sc.capture import base, analysis, util


SOURCE = 'watchdog'


# TODO and what if we define our model here instead? hmm...
# maybe define it here, and register it somewhere?
# maybe using the famed Source singleton? hmm...
class WatchdogRawEvent(models.RawEvent):
    "Events produced by collecting data produced by a Watchdog source."

    event_data = models.fields.DictField(required=True)

    objects = models.UtilManager()
    staged = models.StagingManager()
    incremental = models.IncrementalManager(range_field='timestamp_ns')


class Monitor(base.Monitor):
    event_model = WatchdogRawEvent
    handler_cls = _watchdog_events.RegexMatchingEventHandler

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.monitored_dirs = None
        self._observer = None
        self._handler = None

    def configure(self, monitored_dirs=None, recursive=None):

        observer = _watchdog_observers.Observer()
        # monkeypatch the handler's on_any_event() method with a function that stores the event
        self.handler_cls.on_any_event = self.collect_watchdog_event_data

        ignore_regexes = self.get_ignore_regexes(ignore_git=True, ignore_config_dir=True)
        self.log.debug('regexes to ignore: %s', ignore_regexes)

        handler = self.handler_cls(
            ignore_regexes=ignore_regexes,
            ignore_directories=False,
        )

        self.monitored_dirs = monitored_dirs
        assert monitored_dirs, 'At least one monitored dir should be given'

        for path in self.monitored_dirs:
            observer.schedule(handler, os.fspath(path), recursive=recursive)

        self._observer = observer
        self._handler = handler

    def get_ignore_regexes(self, ignore_git=None, ignore_config_dir=None):
        regexes = []
        paths_to_ignore = []

        if ignore_git:
            regexes.append('.*[.]git/.*')

        if ignore_config_dir:
            from sc.config import CONFIG_DIR

            paths_to_ignore.append(CONFIG_DIR)

        for path in paths_to_ignore:
            ignore_path = path.absolute()
            # windows paths cause all sorts of problems with regexes,
            # so for the time being we just use the name
            regex = f'.*/{ignore_path.name}/.*'
            regexes.append(regex)

        return regexes

    def collect_watchdog_event_data(self, watchdog_event):
        event_data = {
            'event_type': watchdog_event.event_type,
            'src_path': watchdog_event.src_path,
            # TODO is there a corresponding dest_path?
            'is_directory': watchdog_event.is_directory,
        }

        event = self.event_manager.create(
            timestamp_ns=time.time_ns(),
            event_data=event_data
        )

    def start(self):
        self.log.info('Starting Watchdog observer')
        self._observer.start()

    def stop(self, timeout=1):
        self.log.info('Stopping Watchdog observer')
        self._observer.join(timeout)
        self._observer.stop()


class ProcessRawEvents(analysis.LinearPipeline):

    @property
    def event_cls(self):
        return self.params.get('event_cls')

    @property
    def default_steps(self):
        return [
            self.assign_fields,
            self.coalesce_repeated,
            self.assign_event_properties,
            self.select_relevant,
            self.create_event_objects,
        ]

    class c:
        timestamp_ns = WatchdogRawEvent.timestamp_ns.attname
        event_data = WatchdogRawEvent.event_data.attname
        src_path = 'src_path'
        is_directory = 'is_directory'
        event_type = 'event_type'
        ts = 'ts'
        path = 'path'
        action = 'action'
        artifact_type = 'artifact_type'

    def _get_event_data_as_dataframe(self, s):
        return pd.DataFrame(s.to_list(), index=s.index)

    def assign_fields(self, df):
        event_data = (df
                      [self.c.event_data]
                      .pipe(self._get_event_data_as_dataframe)
                     )

        assigns = {
            self.c.ts: lambda d: d[self.c.timestamp_ns].pipe(util.get_timestamp_from_epoch, unit='ns'),
            self.c.event_type: event_data[self.c.event_type],
            self.c.path: event_data[self.c.src_path],
            self.c.is_directory: event_data[self.c.is_directory]
        }

        raw_fields = [
            self.c.timestamp_ns,
            self.c.event_data
        ]

        return (df
                .assign(**assigns)
                .drop(columns=raw_fields)
               )

    def coalesce_repeated(self, df):
        coalesce = util.CoalesceAdjacent(
            distinct=[self.c.path, self.c.event_type, self.c.is_directory],
            timestamp_column=self.c.ts,
            timedelta_max=0.2
        )

        return (df
                .sort_values([self.c.path, self.c.ts])
                .pipe(coalesce)
                .sort_values(self.c.ts)
               )

    def assign_event_properties(self, df):
        df = df.copy()

        ev_type = df[self.c.event_type]
        is_dir = df[self.c.is_directory]

        fs = models.filesystem

        df.loc[ev_type == 'created', self.c.action] = fs.Action.create
        df.loc[ev_type == 'deleted', self.c.action] = fs.Action.delete
        df.loc[ev_type == 'moved', self.c.action] = fs.Action.move
        df.loc[ev_type == 'modified', self.c.action] = fs.Action.write

        df.loc[~is_dir, self.c.artifact_type] = fs.ArtifactType.file
        df.loc[is_dir, self.c.artifact_type] = fs.ArtifactType.directory

        return df

    def select_relevant(self, df):

        # Watchdog generates "dir modified" events whenever a directory's contents are modified
        # this is in almost all cases redundant, and so they are filtered out
        dir_modified = df[self.c.is_directory] & (df[self.c.event_type] == 'modified')

        is_relevant = ~(dir_modified)

        fields_requiring_value = [self.c.path, self.c.action, self.c.artifact_type]

        return (df
                [is_relevant]
                .dropna(subset=fields_requiring_value)
               )

    def get_event_from_dataframe_row(self, row, event_cls=None):
        # NOTE in principle event_cls could also be decided here, depending on the data
        e = event_cls()

        e.time = row[self.c.ts]
        # this should be changed with "duration"
        # e.interval_type = row[self.c.position_in_chunk]
        e.duration = row['duration'].total_seconds()

        e.action = row[self.c.action]
        e.artifact_type = row[self.c.artifact_type]

        # e.artifact = {
        #     'type': e.artifact_type,
        #     'path': row[self.c.path]
        # }
        e.artifact.type = e.artifact_type
        e.artifact.path = row[self.c.path]

        e.source = SOURCE

        return e

    def create_event_objects(self, df):
        return util.create_objects_from_dataframe(
            df,
            self.get_event_from_dataframe_row,
            event_cls=self.event_cls
        )


class Analyzer(analysis.RawEventsAnalyzer):
    raw_event_model = WatchdogRawEvent
    event_model = models.FilesystemEvent

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.processor = ProcessRawEvents(event_cls=self.event_model)


class Adaptor(base.Adaptor):
    source = SOURCE

    def __init__(self, *args, monitor=None, ingester=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.monitor = monitor or Monitor()
        self.ingester = ingester or Analyzer()

    def configure(self, settings):
        self.monitor.configure(
            monitored_dirs=settings.monitored_dirs,
            recursive=settings.recursive
        )
