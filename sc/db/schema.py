"""
DB schema
"""


class MetadataDatabase(object):
    DB = 'sc'
    EVENTS = 'events'
    CAPSULES = 'capsules'


# SQLite: snapshot related metadata
class SnapshotAttribute(object):
    NAME = 'name'
    ALIAS = 'alias'
    PARENT = 'parent'
    TIME = 'time'
    BRANCH = 'branch'
    STATUS = 'status' # A: active, D: deactivated/deleted

class SnapshotTree(object):
    BRANCH = 'branch'
    CURRENT = 'current'

class SnapshotsDatabase(object):
    DB = 'sc_snapshots.db'
    SNAPSHOTS = 'snapshots'
    BRANCHES = 'branches'


# TODO: implement and use the capsule metadata collection
class CapsuleAttribute(object):
    USER = 'user'
    CONTEXT = 'context'
    #CURRENT = 'current'
    #TRUNK = 'trunk'
