
## Using Science Capsule running on bare-metal

Make sure that `SC_CONFIG_DIR` is set. Please refer to the [installation](README-baremetal.md) guide for setting up the configuration directory. If the configuration directory is set, then `sc` commands can be issued from any directory.

```sh
export SC_CONFIG_DIR="$HOME/.scicap"

sc services start all
```

If one or more services encounter errors during startup,
a warning with the location of the log files to be checked for more detailed information will be shown.

If the services are started normally, we can test that events are captured, analyzed, and displayed correctly.

First, generate some test filesystem events:

```sh
# one of the monitored directories
cd $HOME/workdir
echo "very important results" >> doc.txt
```

Then, display the captured events, either by invoking the `sc inspect events` command; or through the WebUI, after refreshing the page. Please refer to the guide for [viewing the workflow timeline](webui.md) for details.
In both cases, it might be necessary to wait a few seconds for the raw events to be saved in the database.

In addition, a real-time view of the event capture service logs can be accessed to display the events as they are being produced:

```sh
sc services tail -f capture
```

## For Science Capsule running inside Docker

Inside the Docker container, filesystem events are automatically captured by Inotify.

Try to run any command that reads, writes, modifies, deletes, or moves files or directories in the monitored `/home/capsule` directory or any of its subdirectories:

```sh
echo "A very important result" > doc.txt
cat doc.txt
echo "An update to the result" >> doc.txt
mkdir -p foo && mv doc.txt foo/
cp foo/doc.txt bar.txt
rm -r foo/doc.txt
```

Then, verify that events are captured and analyzed:

```sh
sc services tail -f capture
```

## Exporting events from the command line

The `sc inspect` commands can be used to display events in a variety of human-readable and structured formats, which can also be used as an impromptu method for e.g. exporting provenance and workflow execution events from the Science Capsule database.

To be able to use the `sc inspect` command, the `mongo` Science Capsule service must be running. See above on how to set up and start Science Capsule services from a new session.

To display a list of e.g. filesystem events in JSON format, run:

```sh
sc inspect events.filesystem --format JSON
```

For a complete list of available options, use the `--help` flag:

```sh
sc inspect --help
```
